{
  send-matrix-message,
  username,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  trade-balance-notify = pkgs.python3.pkgs.buildPythonApplication {
    pname = "trade-balance-notify";
    version = "0.3.9";
    propagatedBuildInputs = [
      pkgs.python3.pkgs.requests
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/trade-balance-notify.git";
      ref = "main";
    };
  };
in
let
  trade-balance-notify-alert = pkgs.writeShellApplication {
    runtimeInputs = [ ];
    name = "trade-balance-notify-alert";
    text = ''
      ${trade-balance-notify}/bin/trade-balance-notify --schedule | \
        ${send-matrix-message}/bin/send-matrix-message --message-type m.text --header "Trade Balance Notification ($(${pkgs.hostname}/bin/hostname))"
    '';
  };
in
{
  environment = {
    systemPackages = with pkgs; [
      "${trade-balance-notify}"
    ];
    shellAliases = {
      trade-balance-notify = "trade-balance-notify --cache";
    };
  };

  systemd = {
    services.trade-balance-notify = {
      enable = true;
      description = "Provide actions and status from simulated and actual trading platforms";
      serviceConfig = {
        User = "${username}";
        ExecStart = "${trade-balance-notify-alert}/bin/trade-balance-notify-alert";
      };
      wants = [ "network-online.target" ];
      after = [ "network-online.target" ];
    };
    timers = {
      trade-balance-notify = {
        enable = true;
        description = "Provide actions and status from simulated and actual trading platforms";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar= "6,20:30:00";
          Unit = "trade-balance-notify.service";
          Persistent = true;
          RandomizedDelaySec = 3600;
        };
      };
    };
  };
}
