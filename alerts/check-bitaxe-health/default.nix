{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
  send-matrix-message,
}:
with lib;
let
  cfg = config.services.check-bitaxe-health;
  check-bitaxe-health = pkgs.python3.pkgs.buildPythonApplication {
    pname = "check-bitaxe-health";
    version = "0.0.1";
    format = "pyproject";
    propagatedBuildInputs = [
      pkgs.python3.pkgs.requests
      pkgs.python3.pkgs.hatchling
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/check-bitaxe-health.git";
    };
  };
in
let
  check-bitaxe-health-alert = pkgs.writeShellApplication {
    runtimeInputs = [
      pkgs.hostname
    ];
    name = "check-bitaxe-health-alert";
    text = ''
      ${check-bitaxe-health}/bin/check-bitaxe-health --host ${cfg.hostname} | \
        ${cfg.notification}
    '';
  };
in
{

  options = {
    services.check-bitaxe-health.enable = mkEnableOption "Enable the check-bitaxe-health service";

    services.check-bitaxe-health.hostname = mkOption {
      type = types.str;
      default = "bitaxe";
      description = "The hostname or IP address of the Bitaxe device to monitor.";
    };
    services.check-bitaxe-health.notification = mkOption {
      type = types.str;
      default = ''
        ${send-matrix-message}/bin/send-matrix-message --message-type m.text --header "Bitaxe Health Check ($(${pkgs.hostname}/bin/hostname))"
      '';
      description = "Bitaxe Health Check script will pipe output to this command";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.check-bitaxe-health = {
      enable = true;
      description = "Monitoring for the bitaxe device";
      serviceConfig = {
        ExecStart = "${check-bitaxe-health-alert}/bin/check-bitaxe-health-alert";
        Type = "simple";
      };
    };
    systemd.timers.check-bitaxe-health = {
      enable = true;
      description = "Monitoring for the bitaxe device";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "hourly";
        Unit = "check-bitaxe-health.service";
      };
    };
  };
}
