{
  send-matrix-message,
  username,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  dns-check = pkgs.python3.pkgs.buildPythonApplication {
    pname = "dns-check";
    version = "0.0.1";
    propagatedBuildInputs = [
      pkgs.python3.pkgs.dnspython
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/dns-check.git";
    };
  };
in
let
  dns-check-alert = pkgs.writeShellApplication {
    runtimeInputs = [ ];
    name = "dns-check-alert";
    text = ''
      ${dns-check}/bin/dns-check | \
        ${send-matrix-message}/bin/send-matrix-message --message-type m.text --header "DNS Check ($(${pkgs.hostname}/bin/hostname))"
    '';
  };
in
{
  environment = {
    systemPackages = with pkgs; [
      "${dns-check}"
    ];
  };

  systemd = {
    services.dns-check = {
      enable = true;
      description = "Test DNS resolving servers";
      serviceConfig = {
        User = "${username}";
        ExecStart = "${dns-check-alert}/bin/dns-check-alert";
      };
      wants = [ "network-online.target" ];
      after = [ "network-online.target" ];
    };
    timers = {
      dns-check = {
        enable = true;
        description = "Test DNS resolving servers";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar= "hourly";
          Unit = "dns-check.service";
          RandomizedDelaySec = 3600;
        };
      };
    };
  };
}
