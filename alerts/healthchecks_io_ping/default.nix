{
  username,
  healthchecks_io_key,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  systemd = {
    services.ping-healthchecks-io = {
      enable = true;
      description = "Update status on healtchchecks.io";
      serviceConfig = {
        User = "${username}";
        ExecStart = "${pkgs.curl}/bin/curl -s https://hc-ping.com/${healthchecks_io_key}";
        Type = "simple";
      };
    };
    timers.ping-healthchecks-io = {
      enable = true;
      description = "Update status on healtchchecks.io";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnBootSec = "1min";
        OnUnitInactiveSec= "45min";
        Unit = "ping-healthchecks-io.service";
      };
    };
  };
}
