{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  networking = {
    nameservers = [ "1.1.1.1" "1.0.0.1" ];
    search = [ "lan" ];
  };
}
