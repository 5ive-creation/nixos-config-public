{
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  networking = {
    nameservers = [ "1.1.1.1" "1.0.0.1" ];
    # Uncomment to completely remove DHCP suggestions
    # dhcpcd.extraConfig = "nohook resolv.conf";
    networkmanager.dns = "none";
    search = [ "lan" ];
  };
}
