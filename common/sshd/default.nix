{
  lib,
  options,
  config,
  specialArgs,
  modulesPath,
}:
{
  services.openssh               = {
    enable                       = true;
    startWhenNeeded              = true;
    openFirewall                 = true;
    settings                     = {
      LogLevel               = "VERBOSE";
      PermitRootLogin        = lib.mkForce "no";
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
    };
  };
}
