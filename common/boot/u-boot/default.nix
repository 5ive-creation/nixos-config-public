{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  boot.loader = {
    grub.enable = false;
    generic-extlinux-compatible.enable = true;
  };
}
