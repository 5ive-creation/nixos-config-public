{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  boot.loader = {
    systemd-boot = {
      enable = true;
      configurationLimit = 50;
    };
    efi.canTouchEfiVariables = true;
  };
}
