{
  pkgs,
  send-matrix-message,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  btrfs-maintenance = pkgs.python3.pkgs.buildPythonApplication {
    pname = "btrfs-maintenance";
    version = "1.0.4";
    propagatedBuildInputs = [ pkgs.btrfs-progs ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/btrfs-maintenance.git";
    };
  };
  btrfs-check-for-corrupted-files = pkgs.writeShellApplication {
    name = "btrfs-check-for-corrupted-files";
    runtimeInputs = [
      pkgs.gnugrep
      pkgs.gawk
      pkgs.systemd
    ];
    text = ''
       ${pkgs.systemd}/bin/journalctl -q --dmesg --grep 'checksum error' | \
       ${pkgs.gnugrep}/bin/grep -v "Journal file" | \
       ${pkgs.gawk}/bin/awk '{ for (i = 31; i <= NF; i++) printf "%s ", $i; printf "\n" }' || \
       ${pkgs.coreutils}/bin/true
    '';
  };
  btrfs-check-for-corrupted-files-alert = pkgs.writeShellApplication {
    name = "btrfs-check-for-corrupted-files-alert";
    runtimeInputs = [
      pkgs.hostname
    ];
    text = ''
      ${btrfs-check-for-corrupted-files}/bin/btrfs-check-for-corrupted-files | \
      ${send-matrix-message}/bin/send-matrix-message --header "Corrupted files on btrfs filesystem ($(${pkgs.hostname}/bin/hostname))" --message-type m.text
    '';
  };
in
{
  systemd = {
    services = {
      btrfs-balance = {
        enable = true;
        description = "Btrfs balance all mounts";
        serviceConfig = {
          ExecStart = "${btrfs-maintenance}/bin/btrfs-balance --verbose -d usage=20 -m usage=20";
        };
      };
      btrfs-balance-full = {
        enable = true;
        description = "Btrfs balance fully all mounts";
        serviceConfig = {
          ExecStart = "${btrfs-maintenance}/bin/btrfs-balance --full --verbose";
        };
      };
      btrfs-check-for-corrupted-files = {
        enable = true;
        description = "Check for any corrupted files outputted to the journal";
        serviceConfig = {
          ExecStart = "${btrfs-check-for-corrupted-files-alert}/bin/btrfs-check-for-corrupted-files-alert";
        };
      };
    };
    timers = {
      btrfs-balance = {
        enable = true;
        description = "Run btrfs balance weekly";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar= "Fri *-*-8..31 3:00:00";
          Unit = "btrfs-balance.service";
        };
      };
      btrfs-balance-full = {
        enable = true;
        description = "Run btrfs full balance";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar= "Fri *-*-1..7 3:00:00";
          Unit = "btrfs-balance-full.service";
        };
      };
      btrfs-check-for-corrupted-files = {
        enable = true;
        description = "Check for any corrupted files outputted to the journal";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = "09:30:00";
          RandomizedDelaySec = "600";
          Unit = "btrfs-check-for-corrupted-files.service";
        };
      };
    };
  };
}
