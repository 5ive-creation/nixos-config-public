{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ./btrfs-maintenance
    ./nix-optimization
    ./swap
  ];
}
