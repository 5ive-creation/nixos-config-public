{
  options,
  modulesPath,
  config,
  specialArgs,
  lib,
}:
{
  nix.settings = {
    auto-optimise-store = true;
    # 10GB
    min-free = toString (10 * 1024 * 1024 * 1024);
    # 100GB
    max-free = toString (100 * 1024 * 1024 * 1024);
  };
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 60d";
  };
  systemd = {
    services = {
      nix-daemon = {
        serviceConfig = {
          MemoryHigh = "500M";
        };
      };
    };
  };
}
