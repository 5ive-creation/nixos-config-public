{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  boot.kernel.sysctl = {
    "vm.swappiness" = 10;
  };
}
