{
  send-matrix-message,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  python311 = pkgs.python311;
  compatible_python = pkgs.python3.version >= "3.11";
  python3 = if compatible_python then pkgs.python3.pkgs else pkgs.python311.pkgs;
in
let
  resource-usage-check = python3.buildPythonApplication {
    pname = "resource-usage-check";
    version = "1.5.6";
    propagatedBuildInputs = [
      python3.psutil
      python3.humanize
      pkgs.btrfs-progs
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/resource-usage-check.git";
    };
  };
in
let
  resource-usage-check-alert = pkgs.writeShellApplication {
    name = "resource-usage-check-alert";
    text = ''
      ${resource-usage-check}/bin/resource-usage-check | \
      ${send-matrix-message}/bin/send-matrix-message --message-type m.text --header "resource-usage-check ($(${pkgs.hostname}/bin/hostname))"
    '';
  };
in
{
  systemd.services.resource-usage-check = {
    enable = true;
    description = "Notify on low disk space";
    serviceConfig = {
      ExecStart = "${resource-usage-check-alert}/bin/resource-usage-check-alert";
    };
  };
  systemd.timers.resource-usage-check = {
    enable = true;
    description = "Notify on low disk space";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnBootSec = "1h";
      OnUnitInactiveSec= "4h";
      Unit = "resource-usage-check.service";
    };
  };
}
