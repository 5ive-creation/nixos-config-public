{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  send-matrix-message = pkgs.python3.pkgs.buildPythonApplication {
    pname = "send-matrix-message";
    version = "1.2.2";
    propagatedBuildInputs = [ pkgs.python3.pkgs.requests ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/send-matrix-message.git";
    };
  };
in
{
  environment.systemPackages = with pkgs; [
    "${send-matrix-message}"
  ];
  _module.args.send-matrix-message = "${send-matrix-message}";
}
