{
  send-matrix-message,
  username,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  failed-systemd-units-alert = pkgs.writeShellApplication {
    name = "failed-systemd-units-alert";
    text = ''
      ${pkgs.systemd}/bin/systemctl list-units --failed --quiet --plain --no-pager | ${pkgs.gawk}/bin/awk '{print $1;}' | \
        ${send-matrix-message}/bin/send-matrix-message --message-type m.text --header "Failed systemd Units ($(${pkgs.hostname}/bin/hostname))"
    '';
  };
in
{
  systemd = {
    services.failed-systemd-units-alert = {
      enable = true;
      description = "Notify failed systemd units";
      serviceConfig = {
        User = "${username}";
        ExecStart = "${failed-systemd-units-alert}/bin/failed-systemd-units-alert";
      };
    };
    timers.failed-systemd-units-alert = {
      enable = true;
      description = "Notify failed systemd units";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnBootSec = "2h";
        OnUnitInactiveSec= "4h";
        Unit = "failed-systemd-units-alert.service";
      };
    };
  };
}
