{
  send-matrix-message,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  refused-sshd-connection = pkgs.writeShellApplication {
    name = "refused-sshd-connection";
    runtimeInputs = [
      pkgs.systemd
      pkgs.gnugrep
    ];
    text = ''
      ${pkgs.systemd}/bin/systemctl status --quiet --plain --no-pager sshd.socket | ${pkgs.gnugrep}/bin/grep Refused 2>&1 || ${pkgs.coreutils}/bin/true
    '';
  };
in
let
  refused-sshd-connection-alert = pkgs.writeShellApplication {
    name = "refused-sshd-connection-alert";
    text = ''
      ${refused-sshd-connection}/bin/refused-sshd-connection | \
      ${send-matrix-message}/bin/send-matrix-message --message-type m.text --header "refused sshd connections present ($(${pkgs.hostname}/bin/hostname))"
    '';
  };
in
{
  systemd.services.refused-sshd-connection-alert = {
    enable        = true;
    description   = "Notify when SSH connections are refused";
    serviceConfig = {
      ExecStart   = "${refused-sshd-connection-alert}/bin/refused-sshd-connection-alert";
    };
  };
  systemd.timers.refused-sshd-connection-alert = {
    enable      = true;
    description = "Notify when SSH connections are refused";
    wantedBy    = [ "timers.target" ];
    timerConfig = {
      OnBootSec          = "1h";
      OnUnitInactiveSec  = "4h";
      RandomizedDelaySec = "300";
      Unit               = "refused-sshd-connection-alert.service";
    };
  };
}
