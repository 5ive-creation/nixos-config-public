{
  send-matrix-message,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  release_manual = builtins.fetchurl {
    url = "https://nixos.org/manual/nixos/stable/";
  };
in
let
  get-stable-nixos-version = pkgs.writeShellApplication {
      name = "get-stable-nixos-version";
      runtimeInputs = [
        release_manual
        pkgs.gnugrep
      ];
      text = ''
        ${pkgs.gnugrep}/bin/grep -oP '(?<="channel":"stable","version":")[0-9]+\.[0-9]+(?=")' ${release_manual}
      '';
  };

in
let
  channel_url = "https://nixos.org/channels/nixos-";
  check-nixos-channel = pkgs.writeShellApplication {
      name = "check-nixos-channel";
      runtimeInputs = [
        get-stable-nixos-version
        pkgs.coreutils
        pkgs.gnugrep
        pkgs.nix
      ];
      text = ''
        ${pkgs.nix}/bin/nix-channel --list | \
        ${pkgs.gnugrep}/bin/grep -q "^nixos ${channel_url}$(${get-stable-nixos-version}/bin/get-stable-nixos-version)$" || \
          (${pkgs.coreutils}/bin/echo "Current channel: "; ${pkgs.nix}/bin/nix-channel --list)
      '';
  };
in
let
  check-nixos-channel-alert = pkgs.writeShellApplication {
      name = "check-nixos-channel-alert";
      text = ''
        ${check-nixos-channel}/bin/check-nixos-channel | \
        ${send-matrix-message}/bin/send-matrix-message \
          --header "NixOS not using desired channel ($(${pkgs.hostname}/bin/hostname))" \
          --footer "Update with: sudo nix-channel --add ${channel_url}$(${get-stable-nixos-version}/bin/get-stable-nixos-version) nixos<br/> \
                    Refer to release notes for incompatibilities: https://nixos.org/manual/nixos/stable/release-notes.html#sec-release-$(${get-stable-nixos-version}/bin/get-stable-nixos-version)" \
          --message-type m.text
      '';
  };
in
{
  systemd.services.check-nixos-channel = {
    enable = true;
    description = "Check if NixOS is using desired channel";
    serviceConfig = {
      ExecStart = "${check-nixos-channel-alert}/bin/check-nixos-channel-alert";
      Type = "simple";
    };
  };
  systemd.timers.check-nixos-channel = {
    enable = true;
    description = "Check if NixOS is using desired channel";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnCalendar = "09:00:00";
      RandomizedDelaySec = "3600";
      Unit = "check-nixos-channel.service";
    };
  };
}
