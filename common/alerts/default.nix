{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ./check-nixos-channel
    ./check-nixos-rebuild-date
    ./check-nixos-requires-reboot
    ./failed-systemd-units-alert
    ./refused-sshd-connection-alert
    ./resource-usage-check
    ./send-matrix-message
  ];
}
