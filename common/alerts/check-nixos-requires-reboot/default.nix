{
  pkgs,
  username,
  send-matrix-message,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  check-nixos-requires-reboot = pkgs.writeShellApplication { 
    name = "check-nixos-requires-reboot";

    runtimeInputs = [
      pkgs.coreutils
      pkgs.gawk
      pkgs.gnugrep
      pkgs.hostname
    ];
    text = ''
      booted="$(${pkgs.coreutils}/bin/readlink /run/booted-system/{initrd,kernel,kernel-modules})"
      built="$(${pkgs.coreutils}/bin/readlink /nix/var/nix/profiles/system/{initrd,kernel,kernel-modules})"

      if [[ "$booted" == "$built" ]]; then
        exit 0
      fi

      for i in $(${pkgs.coreutils}/bin/ls /nix/var/nix/profiles/system/specialisation/); do \
        built="$(${pkgs.coreutils}/bin/readlink /nix/var/nix/profiles/system/specialisation/"$i"/{initrd,kernel,kernel-modules})";
        if [[ "$booted" == "$built" ]]; then \
          exit 0
        fi
      done

      ${pkgs.coreutils}/bin/echo "System requires reboot to take advantage of rebuilt profile ($(${pkgs.hostname}/bin/hostname))"
      ${pkgs.coreutils}/bin/true
    '';
  };
in
let
  check-nixos-requires-reboot-alert = pkgs.writeShellApplication {
      name = "check-nixos-requires-reboot-alert";
      text = ''
        ${check-nixos-requires-reboot}/bin/check-nixos-requires-reboot | \
        ${send-matrix-message}/bin/send-matrix-message \
          --header "System requires reboot ($(${pkgs.hostname}/bin/hostname))" \
          --message-type m.text
      '';
  };
in
{
  systemd.services.check-nixos-requires-reboot = {
    enable = true;
    description = "Check whether the NixOS system requires a reboot";
    serviceConfig = {
      ExecStart = "${check-nixos-requires-reboot-alert}/bin/check-nixos-requires-reboot-alert";
      Type = "simple";
      User = "${username}";
    };
  };
  systemd.timers.check-nixos-requires-reboot = {
    enable = true;
    description = "Check whether the NixOS system requires a reboot";
    wantedBy = [ "timers.target" ];
    timerConfig         = {
      OnBootSec         = "24h";
      OnUnitInactiveSec = "48h";
      Unit              = "check-nixos-requires-reboot.service";
    };
  };
}
