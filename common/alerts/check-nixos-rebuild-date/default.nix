#
# This will most likely be deprecated in favor of doing auto-upgrades
#
{
  pkgs,
  send-matrix-message,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  check-nixos-rebuild-date = pkgs.writeShellApplication { 
    name = "check-nixos-rebuild-date";

    runtimeInputs = [
      pkgs.coreutils
      pkgs.gawk
      pkgs.gnugrep
      pkgs.hostname
    ];

    text = ''
      THRESHOLD=30
      BUILT_ON=$(${pkgs.coreutils}/bin/date +%s -d "$(${pkgs.gnugrep}/bin/grep -Po "Built on .+" "/boot/loader/entries/$(${pkgs.gnugrep}/bin/grep -Po "default .+" /boot/loader/loader.conf | ${pkgs.gawk}/bin/awk '{print $2;}')" | ${pkgs.gawk}/bin/awk '{print $3;}')")
      CURRENT_DATE=$(${pkgs.coreutils}/bin/date +%s)
      DIFF=$((CURRENT_DATE - BUILT_ON))
      DAYS_SINCE_LAST_REBUILD=$((DIFF / 86400))
      if [ $DAYS_SINCE_LAST_REBUILD -ge $THRESHOLD ]
      then
        ${pkgs.coreutils}/bin/echo "System hasn't been rebuilt for $DAYS_SINCE_LAST_REBUILD days ($(${pkgs.hostname}/bin/hostname))"
      fi
      ${pkgs.coreutils}/bin/true
    '';
  };
in
let
  check-nixos-rebuild-date-alert = pkgs.writeShellApplication {
      name = "check-nixos-rebuild-date-alert";
      text = ''
        ${check-nixos-rebuild-date}/bin/check-nixos-rebuild-date | \
        ${send-matrix-message}/bin/send-matrix-message \
          --header "System has not been rebuilt recently ($(${pkgs.hostname}/bin/hostname))" \
          --message-type m.text
      '';
  };
in
{
  systemd.services.check-nixos-rebuild-date = {
    enable = true;
    description = "Check when the NixOS system was last rebuilt";
    serviceConfig = {
      ExecStart = "${check-nixos-rebuild-date-alert}/bin/check-nixos-rebuild-date-alert";
      Type = "simple";
    };
  };
  systemd.timers.check-nixos-rebuild-date = {
    enable = true;
    description = "Check when the NixOS system was last rebuilt";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnCalendar = "weekly";
      Unit = "check-nixos-rebuild-date.service";
    };
  };
}
