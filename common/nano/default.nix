{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  programs.nano.nanorc = "
    set tabstospaces
    set tabsize 4
  ";
}
