{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  programs = {
    bash = {
      interactiveShellInit = (builtins.readFile ./tmux-attach);
    };
    tmux = {
      enable = true;
      extraConfig = ''
        set -g set-titles on
      '';
    };
  };
}
