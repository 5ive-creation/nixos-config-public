{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  systemd = {
    services = {
      NetworkManager-wait-online = {
        enable = false;
      };
    };
  };
}
