{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  btr-backup = pkgs.python3.pkgs.buildPythonApplication {
    pname = "btr-backup";
    version = "2.4.7";
    propagatedBuildInputs = [
      pkgs.util-linuxMinimal
      pkgs.rsync
      pkgs.btrfs-progs
      pkgs.xz
      pkgs.coreutils-full
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/btr-backup.git";
    };
  };
in
{
  environment.systemPackages = with pkgs; [
    "${btr-backup}"
  ];

  systemd.services.btr-backup = {
    enable = true;
    description = "Run btr-backup on btrfs volumes";
    serviceConfig = {
      ExecStart = "${btr-backup}/bin/btr-backup";
      Type = "simple";
    };
  };
  systemd.timers.btr-backup = {
    enable = true;
    description = "Run btr-backup on btrfs volumes";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnCalendar = "2:00:00";
      Persistent = true;
      Unit = "btr-backup.service";
    };
  };
}

