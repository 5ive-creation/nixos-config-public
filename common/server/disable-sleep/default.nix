{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  systemd.targets = {
    sleep.enable = false;
    suspend.enable = false;
    hibernate.enable = false;
    hybrid-sleep.enable = false;
  };
}

