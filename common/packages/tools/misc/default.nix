{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  environment.systemPackages = with pkgs; [
    dua
    nixfmt-rfc-style
  ];
}
