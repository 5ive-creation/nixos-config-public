{
  username,
  git_username,
  git_email,
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
let
  git_config_content = pkgs.writeTextFile {
    name = "git_config_${username}";
    text = ''
      [user]
        email = "${git_email}"
        name = "${git_username}"
    '';
  };
  ssh_config_content = pkgs.writeTextFile {
    name = "ssh_config_${username}";
    text = ''
      Host *
      ForwardAgent yes
      ServerAliveInterval 240
      ConnectionAttempts 50
    '';
  };
in
{

  users.users.${username} = {
     isNormalUser = true;
     extraGroups = [
       "wheel"
     ];
     home = "/home/${username}";
  };

  system.activationScripts."git_config_${username}" = {
    text = ''
      mkdir -p /home/${username}/.config/git
      chown -R ${username} /home/${username}/.config
      ln -sf ${git_config_content} /home/${username}/.config/git/config
    '';
  };
  system.activationScripts."ssh_config_${username}" = {
    text = ''
      mkdir -p /home/${username}/.ssh
      chown -R ${username} /home/${username}/.ssh
      ln -sf ${ssh_config_content} /home/${username}/.ssh/config
    '';
  };

}
