{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
  pkgs,
  send-matrix-message,
}:
let
  fwupd-firmware-updates-alert = pkgs.writeShellApplication {
    runtimeInputs = [
      pkgs.fwupd
      pkgs.coreutils
      pkgs.jq
    ];
    name = "fwupd-firmware-updates-alert";
    text = ''
      if [ "$(${pkgs.fwupd}/bin/fwupdmgr get-updates --json |${pkgs.jq}/bin/jq '.Devices | length')" -gt 0 ]; then
        ${pkgs.coreutils}/bin/echo "Updates Available" | \
          ${send-matrix-message}/bin/send-matrix-message --message-type m.text --header "fwupd Firmware Updates Notification ($(${pkgs.hostname}/bin/hostname))"
      fi
    '';
  };
in
{
  services.fwupd.enable = true;
  systemd = {
    services = {
      fwupd-refresh = {
        serviceConfig = {
          Restart = "on-failure";
          RestartSec = 10;
          StartLimitBurst = 5;
        };
        unitConfig = {
          StartLimitInterval = 0;
        };
      };
      fwupd-firmware-updates = {
        enable = true;
        description = "Check fwupd for firmware updates";
        serviceConfig = {
          ExecStart = "${fwupd-firmware-updates-alert}/bin/fwupd-firmware-updates-alert";
        };
        wants = [ "network-online.target" ];
        after = [ "network-online.target" ];
      };
    };
    timers = {
      fwupd-firmware-updates = {
        enable = true;
        description = "Check fwupd for firmware updates";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = "daily";
          Unit = "fwupd-firmware-updates.service";
          RandomizedDelaySec = 3600;
        };
      };
    };
  };
}
