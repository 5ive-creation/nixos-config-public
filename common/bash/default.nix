{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  programs.bash = {
    interactiveShellInit = "
      HISTCONTROL=ignoreboth
    ";
  };
}
