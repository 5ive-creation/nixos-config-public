{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  public_config = builtins.fetchGit {
    url = "https://gitlab.com/5ive-creation/nixos-config-public.git";
    ref = "main";
  };
in
{
  specialisation = {
    remote.configuration = {
      system.nixos.tags = [ "remote" ];
      imports = [
        "${public_config}/common/dns/cloudflare"
      ];
    };
  };
}
