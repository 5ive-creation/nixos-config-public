{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  public_config = builtins.fetchGit {
    url = "https://gitlab.com/5ive-creation/nixos-config-public.git";
    ref = "main";
  };
in
{
  specialisation = {
    include_local.configuration = {
      system.nixos.tags = [ "include_local" ];
      imports = [
        "${public_config}/common/dns/cloudflare/include_local"
      ];
    };
  };
}
