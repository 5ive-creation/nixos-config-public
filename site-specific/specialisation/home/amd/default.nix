{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  public_config = builtins.fetchGit {
    url = "https://gitlab.com/5ive-creation/nixos-config-public.git";
    ref = "main";
  };
in
{
  specialisation = {
    amd.configuration = {
      system.nixos.tags = [ "amd" ];
      boot = {
        initrd.kernelModules = [ "amdgpu" ];
        kernelParams = [ "module_blacklist=i915" ];
      };
      services.xserver.videoDrivers = [ "amdgpu" ];
      imports = [
        "${public_config}/site-specific/nix-binary-cache"
      ];
    };
  };
}
