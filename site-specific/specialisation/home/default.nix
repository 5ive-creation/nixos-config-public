{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  public_config = builtins.fetchGit {
    url = "https://gitlab.com/5ive-creation/nixos-config-public.git";
    ref = "main";
  };
in
{
  specialisation = {
    home.configuration = {
      system.nixos.tags = [ "home" ];
      imports = [
        "${public_config}/site-specific/nix-binary-cache"
      ];
    };
  };
}
