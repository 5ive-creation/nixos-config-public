{
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
}:
{
  imports = [
    ../../pihole
  ];
  environment.etc = {
    "dnsmasq.d/02-pihole-dhcp-custom.conf" = {
      mode = "0444";
      text = ''
        dhcp-option=option:dns-server,192.168.86.33,192.168.86.3
      '';
    };
  };
}

