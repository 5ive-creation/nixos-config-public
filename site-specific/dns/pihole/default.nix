{
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  environment.etc = {
    "pihole/custom.list" = {
      mode = "0444";
      source = ./custom.list;
    };
  };

  #  system.activationScripts.assign-roku-group = {
  #    text = ''
  #      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "INSERT OR IGNORE INTO 'group' (id, name) VALUES (1, 'ROKU');" || true
  #      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "INSERT OR IGNORE INTO client (id, ip) VALUES (1, '192.168.86.32');" || true
  #      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "INSERT OR IGNORE INTO client_by_group (client_id, group_id) VALUES (1, 1);" || true
  #      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "DELETE FROM client_by_group WHERE client_id = 1 AND group_id = 0;" || true
  #    '';
  #  };

  system.activationScripts.allow-for-tlcgo = {
    text = ''
      if [ ! -e /etc/pihole/gravity.db ]; then
        exit 0
      fi

      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "INSERT OR IGNORE INTO 'domainlist' (id, type, domain, enabled, date_added, date_modified, comment) VALUES (1, 0, 'geolocation.onetrust.com', 1, 1701057748, 1701057748, 'required for TLCgo');" || true
    '';
  };
}
