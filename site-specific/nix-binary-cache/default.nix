{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  nix.settings = {
    substituters = [
      "https://cache.ninjalasers.com/"
    ];
    trusted-public-keys = [
      "cache.ninjalasers.com:kSphf9ln8UViEqJW45zhRavf8V8JiQSIhuNqmIqFavo="
    ];
  };
}
