{
  username,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  users.users.${username} = {
     openssh.authorizedKeys.keys = [
       "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAt1iFKVln9z6YTK+U0NU1askAYyvrYyovrT4qMxqmyExCuXKTZ6jpc0BsaiV2ENR/olLtWa47KePgFA5rQS32mPSuMZOKY2CaUciXmb6y26PNbterISaT4P63kN2L+Ydt5n2tjXyBLzCkjV7XdUJEzVQWLgu3iaxlaPOG1ViTbbd5cv2HdszrDVzCPUP3ly+nrN0ts7m/vnCrmB3PRPUMYObNVfvbCTpAx1+QsWGjqwytnkoSueojl/Zl/2YxO4nU0HKa5XiAi+4Ab/0fuOxHa8f1G0tokf1/C/oKKrADysidbWeBe7xtr/AcTdspbcay8TT97cqYcH+p3TR5AbgOUw== mogged-iPad-20170612"
       "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAtTDqev5xJ4ONL0s3Kd4qbGOK+tF7RJ3I6CqyPLx+NhKWl2lasQdXRnjYpYq2yREZmK9J9v+7B4YftQEx0DNa/7R2VWBBpDCVfYvtjv7U0WUzGrXk0FLlv5JnNLTSTRv8kKnmT7QaGQd8YNdbPWUh79gtCnHubP8elT6CALY0UGRoiwcFPG2G+eqOVZ370t0fsYutRFX6poHtaRvC4KhMByd+eLwNKjOTvvCQU5jfVTWUxZ00hh19ZWK12ydUj1vrjlFW/4HrxQwfuXsB6chx/7PRpvOgD8eud5C9MMKjzP8zUyAzLwQ1X21WfgfUpBGMHmlrsYYmhbNtRVCnUZSy+w== mogged-Optiplex3050-20180723"
       "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwYgJsH7+nr4vZtiqDNUg+mSIU2qAsOZ7l/Ajt7Z/49tw+2WdQc8SWMxSOVL+IwFG7ecgQPPWLquhVeLWu0fnDTWwPdRewYRxk5EPe/bV1MNKABaOr+LDkCwzsyPYeP8YUYos7AmHao0VvOYPIjA8MxIdkuIHCt+juwR17s1Gv2xwyx6xk3NKl/fZT9A4jYlvt6+I1FT6gkN6M/YN1fpYK6gEG3HUYv3YhsT3BsTcbZdZQ86pJ34mCkkPKl5UzYyIfTPz30pvJo34cCpUimii3NsCtXkEaG+U80VaYzyVGbBdSn3zlKpAHYi8XU6mWJnTdhYOuQh5kE0MxrbxhXKjbkhTWd+ornwt1UaKbGhVbV7tBbtRzWfI1k6InAY7LcfuEo9/xxo4Oak0DLusTZi1kiCLxr8xwepNAu5N+koA0urn/c4QeLiP+9DNS7k72LXZpp9LFu0cglYmp3yOZJ40ijSp/I5RB2ic09V4ZwEOOY4ujcYLTMEeOfUVT3XaUXJM= mobian@pinephone.lan 2021-03-13"
       "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6CRNXeUh7o42g19xnEra7i0Ydm37PLo3lPwxt39vQ5NEJaXJgvozXcEK2HzKNS/JpsUXA8yVlWxOHAv69t0ox8bKkhDvi0MMmRbOzpwV2obXw+VMG4d6zEXocx5qBaxr/n37lKsXguoKm8WwLdNrAavn+OPC4lyIs6KGQ3PwcOdwWGD3/8vKeNEs+WArAiuLSOISPXUtU0l9+fiiByE9SRsAOgP7/hFW/YlCQIopc6VYl15fOBIQKrU2XVM5EvMPlwfRikUIGQr4QlYaePi+lu1lzLJ1lRrTNpfYC+Nkz+7HVJX3xtNv5gAdi7jrbLiBO/HGhkB+4eYSshTOHhbEV kevinem@pinebook.lan 2021-05-21"
       "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXbejHt6Ky5lxTTwRLBQi8D0CVy2cFAyeun62hguYFb4E7TjH/sqXuYm3B6abNDobb9CBz0C+2uydwTx84zXSXeszYP+JSa2uOyL8sZ7tb6Rlakbrd/BN3fVojBcD+W2LxDkHXHmLNd3zeOpZmdSjkDe8I2QbalGa94rnn4VnQ0mdyd/V52r5uA5YuqmyEFMIQRqAL0kjt5FcB0n80CHOfbMLUK2hPGOcWFIVTUw8EpkpfNf0XB+ljpRKK+3uctT0SG7QFkz9t7BNAsLnA6+8BTq1JjSeCqFizuuI8gWZU5/hhvwyicqmRP+grTTMZc/KlUauHM9JYx0TA9WFUbkNJ kmogged@chromebox 2021-08-06"
       "sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBNUVVhMipk0tsmVnXQuw0Ab8A6Vk6dTV/53Ik8g+ZwdD1qd+2FQGtQKDurxeA1eiRYQ7Xmiw3P7MlshCJnyJWiAAAAAEc3NoOg== kevinem@darp5.lan 2021-08-06"
       "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIFHbhHKWb9DDXJcJh2cjr2GUynlGwIZcR062m1po2c5EAAAABHNzaDo= kmogged yubikey 5c nfc"
       "sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBE+viaDrrjOZRRfjxw85iEtBbnToY31Y3XsFWpzvRTIae/YiCkksn8e0M+0H02awjrGk2oIDNetHJJkhB8FV4l0AAAAEc3NoOg== kmogged yubikey nfc 2020-11-21"
       "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIKWYcHqqG/1Uxa4P9q4yBeEQYW1n92tT3NQVnRfnZ1oHAAAABHNzaDo= kmogged 5c WI 2022-11-10"
       "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAILAqLNgUim44qGm1ZMZO09km74mrVhxomGl/281Rk/Z5AAAABHNzaDo= kmogged 5c nfc white 2023-01-08"
       "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAINHCqUehOCiAdeS0VfW7XfgqIiNgtqMheasEeSe/sFv7AAAABHNzaDo= kmogged yubikey lightning 2023-04-08"
       "sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBOY6h1yzSs1SMwEeif2XtKttYtBph4RQ2bK1KB39Y1Os1kVm+5mpT5Qd4qrEkMxZ0ftgvCU30N0wmxpJsrN4G5wAAAAEc3NoOg== kmogged yubikey no nfc white 2023-10-15"
     ];
     isNormalUser = true;
     home = "/home/${username}";     
  };
}
