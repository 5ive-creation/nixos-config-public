{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  gnome-profile-picture = builtins.fetchGit {
    url = "https://gitlab.com/5ive-creation/nixos-config-public.git";
    ref = "main";
  };
  username = "vacation";
in
let
  gnome-user-config = pkgs.writeTextFile {
    name = "gnome-profile-picture";
    text = ''
      [User]
      Icon = ${gnome-profile-picture}/site-specific/user/${username}/gnome/avatar
      SystemAccount = false
    '';
  };
in
{
  imports = [
    ../../vacation
  ];

  system.activationScripts.gnome-profile-picture = {
    text = ''
      cp --force ${gnome-user-config} /var/lib/AccountsService/users/${username}
    '';
  };
}
