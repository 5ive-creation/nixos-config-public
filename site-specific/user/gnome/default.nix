{
  pkgs,
  username,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  gnome-profile-picture = builtins.fetchGit {
    url = "https://gitlab.com/5ive-creation/nixos-config-public.git";
    ref = "main";
  };
in
let
  gnome-user-config = pkgs.writeTextFile {
    name = "gnome-profile-picture";
    text = ''
      [User]
      Icon = ${gnome-profile-picture}/site-specific/user/gnome/avatar
      SystemAccount = false
    '';
  };
in
{
  imports = [
    ../../../gnome
    ../../user
  ];

  users.users.${username} = {
    extraGroups = [
      "networkmanager"
    ];
  };

  systemd = {
    services.gnome-profile-picture = {
      enable = true;
      description = "Copy missing profile avatar into location that gets wiped with NixoS";
      serviceConfig = {
        ExecStart = "${pkgs.coreutils}/bin/cp --force ${gnome-user-config} /var/lib/AccountsService/users/${username}";
        Type = "oneshot";
      };
      wantedBy = [
        "multi-user.target"
      ];
    };
  };

}
