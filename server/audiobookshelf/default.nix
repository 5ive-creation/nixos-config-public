{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  ## State and settings are in /var/lib/audiobookshelf/
  services = {
    audiobookshelf = {
      enable = true;
      openFirewall = false;
      port = 8234;
    };
  };
}
