{
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  services.fail2ban = {
    enable = true;
    bantime-increment = {
      enable = true;
      overalljails = true;
      maxtime = "9000h";
    };
    ignoreIP = [
      "127.0.0.0/8"
      "192.168.86.0/24"
    ];
    jails = {
      "nginx-botsearch" = ''
        enabled = true
        filter = nginx-botsearch
        findtime = 600
        maxretry = 2
        logpath = /var/log/nginx/access.log
        backend = auto
      '';
      "nginx-http-auth" = ''
        enabled = true
        filter = nginx-http-auth
        findtime = 600
        maxretry = 2
        logpath = /var/log/nginx/access.log
        backend = auto
      '';
      "nginx-bad-ssl" = ''
        enabled = true
        filter = nginx-bad-ssl
        findtime = 600
        maxretry = 1
        backend = systemd
      '';
    };
  };
  environment.etc = {
    "fail2ban/filter.d/nginx-bad-ssl.conf".text = ''
      [Definition]
      failregex = bad key share\) while SSL handshaking, client: <HOST>
      journalmatch = _SYSTEMD_UNIT=nginx.service
    '';
  };
}
