{
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  services.fail2ban = {
    enable = true;
    bantime-increment = {
      enable = true;
      overalljails = true;
      maxtime = "9000h";
    };
    ignoreIP = [
      "127.0.0.0/8"
      "192.168.86.0/24"
    ];
    jails = {
      "oauth2-proxy-rejecting" = ''
        enabled = true
        filter = oauth2-proxy-rejecting
        findtime = 600
        maxretry = 10
        backend = systemd
      '';
      "oauth2-proxy-invalid" = ''
        enabled = true
        filter = oauth2-proxy-invalid
        findtime = 600
        maxretry = 10
        backend = systemd
      '';
    };
  };
  environment.etc = {
    "fail2ban/filter.d/oauth2-proxy-rejecting.conf".text = ''
      [Definition]
      failregex = Rejecting invalid redirect "https://<HOST>
      journalmatch = _SYSTEMD_UNIT=oauth2_proxy.service
    '';
    "fail2ban/filter.d/oauth2-proxy-invalid.conf".text = ''
      [Definition]
      failregex = Invalid redirect provided in X-Auth-Request-Redirect header: https://<HOST>
      journalmatch = _SYSTEMD_UNIT=oauth2_proxy.service
    '';
  };
}
