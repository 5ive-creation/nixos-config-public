{
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  services.fail2ban = {
    enable = true;
    bantime-increment = {
      enable = true;
      overalljails = true;
      maxtime = "9000h";
    };
    ignoreIP = [
      "127.0.0.0/8"
      "192.168.86.0/24"
    ];
    jails = {
      "sshd-negotiate-error" = ''
        enabled = true
        filter = sshd-negotiate-error
        findtime = 600
        maxretry = 1
        backend = systemd
      '';
      "sshd-root-connection" = ''
        enabled = true
        filter = sshd-root-connection
        findtime = 600
        maxretry = 1
        backend = systemd
      '';
      "sshd-root-disconnected" = ''
        enabled = true
        filter = sshd-root-disconnected
        findtime = 600
        maxretry = 1
        backend = systemd
      '';
      "sshd-invalid-user" = ''
        enabled = true
        filter = sshd-invalid-user
        findtime = 600
        maxretry = 2
        backend = systemd
      '';
      "sshd-fatal-timeout" = ''
        enabled = true
        filter = sshd-fatal-timeout
        findtime = 600
        maxretry = 3
        backend = systemd
      '';
    };
  };
  environment.etc = {
    "fail2ban/filter.d/sshd-negotiate-error.conf".text = ''
      [Definition]
      failregex = Unable to negotiate with <HOST>
      journalmatch = _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    '';
    "fail2ban/filter.d/sshd-root-connection.conf".text = ''
      [Definition]
      failregex = Connection closed by authenticating user root <HOST>
      journalmatch = _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    '';
    "fail2ban/filter.d/sshd-root-disconnected.conf".text = ''
      [Definition]
      failregex = Disconnected from authenticating user root <HOST>
      journalmatch = _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    '';
    "fail2ban/filter.d/sshd-invalid-user.conf".text = ''
      [Definition]
      failregex = Invalid user .+ from <HOST>
      journalmatch = _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    '';
    "fail2ban/filter.d/sshd-fatal-timeout.conf".text = ''
      [Definition]
      failregex = fatal: Timeout before authentication for <HOST>
      journalmatch = _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    '';
  };
}
