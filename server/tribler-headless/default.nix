{
  send-matrix-message,
  username,
  config,
  pkgs,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  tribler-queue-manager = pkgs.python3.pkgs.buildPythonApplication {
    pname = "tribler-queue-manager";
    version = "0.2.3";
    propagatedBuildInputs = [
      pkgs.python3.pkgs.requests
      pkgs.python3.pkgs.psutil
      pkgs.rsync
      pkgs.openssh
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/tribler-queue-manager.git";
      ref = "main";
    };
  };
  tribler-headless-watchdog = pkgs.writeShellApplication {
    name = "tribler-headless-watchdog";
    text = ''
      systemctl status tribler-headless | \
        grep -q "Active: active (running)" || echo "Headless tribler is not running ($(${pkgs.hostname}/bin/hostname))" | \
        ${send-matrix-message}/bin/send-matrix-message --message-type m.text
    '';
  };
in
let
  tribler-backup-queue = pkgs.writeShellApplication {
    name = "tribler-backup-queue";
    text = ''
      mkdir -p /home/${username}/.cache/tribler-queue-manager
      tribler_queue_export_file="/home/${username}/.cache/tribler-queue-manager/tribler_queue_export_$(date -I).json"
      ${tribler-queue-manager}/bin/tribler-queue-manager --daemon --export-queue "$tribler_queue_export_file"
    '';
  };
in
{
  nixpkgs.config = {
    packageOverrides = pkgs: {
# UNSTABLE #      unstable = import <nixpkgs-unstable> {
      legacy = import (
        builtins.fetchGit {
          name = "nixos-23.05";
          url = "https://github.com/nixos/nixpkgs/";
          # Commit hash for nixos-unstable
          # `git ls-remote https://github.com/nixos/nixpkgs nixos-unstable`
          ref = "refs/tags/23.05";
          rev = "90d94ea32eed9991e2b8c6a761ccd8145935c57c";
        }){
        config = config.nixpkgs.config;
      };
    };
  };

  environment.systemPackages = with pkgs; [
    "${tribler-queue-manager}"
  ];

 systemd = {
    services.tribler-backup-queue = {
      description = "Backup the Tribler queue";
      serviceConfig = {
        User = "${username}";
        ExecStart = "${tribler-backup-queue}/bin/tribler-backup-queue";
      };
    };
    timers.tribler-backup-queue = {
      enable = true;
      description = "Backup the Tribler queue";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "11:00:00";
        Unit = "tribler-backup-queue.service";
      };
    };
  };

  systemd.services.tribler-headless = {
    enable = true;
    description = "Run tribler headless";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    environment = {
      QT_QPA_PLATFORM = "offscreen";
      CORE_BASE_PATH = "${pkgs.legacy.tribler}";
#      CORE_BASE_PATH = "${pkgs.unstable.tribler}";
#      CORE_BASE_PATH = "${pkgs.tribler}";
      CORE_PROCESS = "1";
      CORE_API_PORT = "52194";
    };
    serviceConfig = {
      User = "${username}";
      ExecStart = "${pkgs.legacy.tribler}/bin/tribler";
#      ExecStart = "${pkgs.unstable.tribler}/bin/tribler";
#      ExecStart = "${pkgs.tribler}/bin/tribler";
      Type = "simple";
      Slice = "tribler-headless.slice";
      Restart = "always";
    };
  };
  systemd.services.tribler-headless-watchdog = {
    enable = true;
    description = "Notify if tribler headless is not running";
    serviceConfig = {
      User = "${username}";
      ExecStart = "${tribler-headless-watchdog}/bin/tribler-headless-watchdog";
    };
  };
  systemd.timers.tribler-headless-watchdog = {
    enable = true;
    description = "Notify if tribler headless is not running";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnBootSec = "1h";
      OnUnitInactiveSec= "1h";
      Unit = "tribler-headless-watchdog.service";
    };
  };
  systemd.services.tribler-queue-manager = {
    enable = true;
    description = "Run tribler queue manager";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      User = "${username}";
      ExecStart = "${tribler-queue-manager}/bin/tribler-queue-manager --loop --daemon";
      Type = "simple";
      Restart = "always";
    };
  };
  networking.firewall = {
    allowedTCPPorts = [
      7759
      7760
    ];
    allowedUDPPorts = [
      7759
      7760
    ];
  };
}
