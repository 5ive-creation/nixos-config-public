{
  pkgs,
  username,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
let
  hang-socket-server = pkgs.python3.pkgs.buildPythonApplication {
    pname = "hang-socket-server";
    version = "1.1.2";
    propagatedBuildInputs = [
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/hang-socket-server.git";
    };
  };
in
{
  networking.firewall = {
    allowedTCPPorts = [
      5000
    ];
  };

  systemd.services.hang-socket-server = {
    enable = true;
    description = "Hang malicious IPs and track";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      User = "${username}";
      ExecStart = "${hang-socket-server}/bin/hang-socket-server";
      Type = "simple";
      Restart = "always";
    };
  };

  services.fail2ban = {
    enable = true;
    bantime-increment = {
      enable = true;
      overalljails = true;
      maxtime = "9000h";
    };
    ignoreIP = [
      "127.0.0.0/8"
      "192.168.86.0/24"
    ];
    jails = {
      "hang-socket-server" = ''
        enabled = true
        filter = hang-socket-server
        findtime = 600
        maxretry = 1
        backend = systemd[journalflags=1]
        ignoreregex =
      '';
    };
  };

  environment.etc = {
    "fail2ban/filter.d/hang-socket-server.conf".text = ''
      [Definition]
      failregex = Connection from: \('<HOST>
      journalmatch = _SYSTEMD_UNIT=hang-socket-server.service 
    '';
  };
}
