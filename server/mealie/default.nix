{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  ## State and settings are in /var/lib/mealie/
  services = {
    mealie = {
      enable = true;
      port = 9000;
      settings = {
        TZ = "US/Central";
        ALLOW_SIGNUP = "false";
      };
    };
  };
}
