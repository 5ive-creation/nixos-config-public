{
  pkgs,
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
  domain,
}:
{

  imports = [
    ../letsencrypt
  ];

  #
  # Access the running container with:
  # podman exec -it albyhub /bin/bash
  #
  virtualisation.oci-containers = {
    backend = "podman";
    containers = {
      albyhub = {
        image = "ghcr.io/getalby/hub:latest";
        ports = [ "8080:8080/tcp" ];
        environment = {
          WORK_DIR = "/var/lib/albyhub";
        };
        volumes = [
          "/var/lib/albyhub:/var/lib/albyhub"
        ];
      };
    };
  };

  system.activationScripts.init-albyhub-directory = {
    text = ''
      mkdir -p /var/lib/albyhub
    '';
  };

  system.activationScripts.update-albyhub-version = {
    text = ''
      ${pkgs.podman}/bin/podman pull ghcr.io/getalby/hub || true
    '';
  };

  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    commonHttpConfig = ''
      map $scheme $hsts_header {
          https   "max-age=31536000; includeSubdomains; preload";
      }
      map $proxy_add_x_forwarded_for $client_ip {
          "~(?<IP>([0-9]{1,3}\.){3}[0-9]{1,3}),.*" $IP;
      }
    '';
    virtualHosts."alby.${domain}" = {
      enableACME = true;
      acmeRoot = null;
      forceSSL = true;

      extraConfig = ''
        access_log /var/log/nginx/alby.${domain}_access.log;
        error_log /var/log/nginx/alby.${domain}_error.log;
      '';

      locations."/" = {
        proxyPass = "http://localhost:8080";
        proxyWebsockets = true;
        extraConfig = ''
          add_header Strict-Transport-Security $hsts_header;
          proxy_ssl_server_name on;
          proxy_headers_hash_max_size 512;
          proxy_headers_hash_bucket_size 128;
        '';
      };
    };
  };
}
