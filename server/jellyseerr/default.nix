{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  ## State and settings are in /var/lib/jellyseerr/
  services.jellyseerr.enable = true;
}
