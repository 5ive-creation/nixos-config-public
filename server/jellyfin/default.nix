{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  services.jellyfin.enable = true;
  boot = {
    kernel = {
      sysctl = {
        "fs.inotify.max_user_instances" = 512;
      };
    };
  };
  networking.firewall = {
    allowedTCPPorts = [
      8096
    ];
    allowedUDPPorts = [
      1900
      7359
    ];
  };
}
