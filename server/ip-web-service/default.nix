{
  acme_email,
  acme_dns_provider,
  acme_credentials_file,
  ip_web_service_hostname,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedTlsSettings = true;

    virtualHosts = {
      "${ip_web_service_hostname}" = {
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
        locations."/" = {
          extraConfig = ''
            add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload";
            add_header X-Frame-Options DENY;
            add_header X-Content-Type-Options nosniff;
            add_header X-XSS-Protection "1; mode=block";
            default_type text/plain;

            access_log /var/log/nginx/${ip_web_service_hostname}_access.log;
            error_log /var/log/nginx/${ip_web_service_hostname}_error.log;
          '';
          return = "200 $remote_addr";
        };
      };
    };
  };
  networking.firewall = {
    allowedTCPPorts = [
      443
    ];
  };
  users.users.nginx.extraGroups = [ "acme" ];
  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "${acme_email}";
      dnsProvider = "${acme_dns_provider}";
      credentialsFile = "${acme_credentials_file}";
    };
  };
}
