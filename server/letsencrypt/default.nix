{
  pkgs,
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
  domain,
}:
{
  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "letsencrypt@${domain}";
      dnsProvider = "cloudflare";
      credentialsFile = "/var/lib/letsencrypt/cloudflare.ini";
    };
  };
  users.users.nginx.extraGroups = [ "acme" ];

  networking.firewall = {
    allowedTCPPorts = [
      443
    ];
  };

}
