{
  send-matrix-message,
  pkgs,
  username,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  update-cloudflare-dns = pkgs.python3.pkgs.buildPythonApplication {
    pname = "update-cloudflare-dns";
    version = "1.0.3";
    propagatedBuildInputs = [
      pkgs.python3.pkgs.requests
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/update-cloudflare-dns.git";
    };
  };
in
let
  update-cloudflare-dns-alert = pkgs.writeShellApplication {
    name = "update-cloudflare-dns-alert";
    text = ''
      ${update-cloudflare-dns}/bin/update-cloudflare-dns --quiet | \
        ${send-matrix-message}/bin/send-matrix-message --header "update-cloudflare-dyn-dns ($(${pkgs.hostname}/bin/hostname))" --message-type m.text
    '';
  };
in
{

  systemd = {
    services.update-cloudflare-dns = {
      enable = true;
      description = "Update DNS record on Cloudflare";
      serviceConfig = {
        User = "${username}";
        ExecStart = "${update-cloudflare-dns-alert}/bin/update-cloudflare-dns-alert";
        Type = "simple";
      };
    };
    timers.update-cloudflare-dns = {
      enable = true;
      description = "Update DNS record on Cloudflare";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*:56:00";
        Unit = "update-cloudflare-dns.service";
      };
    };
  };
}
