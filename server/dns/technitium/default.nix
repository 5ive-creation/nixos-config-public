{
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
}:
{
  networking = {
    nameservers = [
      "127.0.0.1"
      "1.1.1.1"
      "1.0.0.1"
    ];
  };

  services.technitium-dns-server = {
    enable = true;
    openFirewall = true;
  };
}
