{
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
}:
{

  imports = [
    ../../technitium
  ];

  networking = {
    firewall = {
      allowedUDPPorts = [
        67
      ];
    };
  };

  services.technitium-dns-server = {
    enable = true;
    openFirewall = true;
  };
}
