{
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
}:
{
  imports = [
    ../../pihole
  ];

  virtualisation.oci-containers = {
    containers = {
      pihole = {
        extraOptions = [
          "--network=host"
        ];
      };
    };
  };

}
