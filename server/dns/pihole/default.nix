{
  pkgs,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  #
  # Access the running container with:
  # podman exec -it pihole /bin/bash
  #
  virtualisation.oci-containers = {
    backend = "podman";
    containers = {
      pihole = {
        image = "pihole/pihole:latest";
        ports = [
          "53:53/tcp"
          "53:53/udp"
          "8314:80/tcp"
        ];
        environment = {
          INSTALL_WEB_INTERFACE = "true";
          WEBTHEME = "default-darker";
          TEMPERATUREUNIT = "f";
          QUERY_LOGGING = "true";
          DNSMASQ_LISTENING = "all";
          TZ = "America/Chicago";
          PIHOLE_DNS_ = "1.1.1.1;1.0.0.1;8.8.8.8;8.8.4.4";
        };
        volumes = [
          "/etc/pihole:/etc/pihole"
          "/etc/dnsmasq.d:/etc/dnsmasq.d"
        ];
        # extraOptions = [ "--cap-add=NET_ADMIN" "--network=host" "--restart=unless-stopped"];
        extraOptions = [
          "--cap-add=NET_ADMIN"
          "--cap-add=NET_RAW"
        ];
      };
    };
  };
  networking = {
    nameservers = [
      "127.0.0.1"
      "1.1.1.1"
      "1.0.0.1"
    ];
    #    dhcpcd.extraConfig = "nohook resolv.conf";
    #    networkmanager.dns = "none";
    search = [ "lan" ];
    firewall = {
      allowedTCPPorts = [
        53
        80
      ];
      allowedUDPPorts = [
        53
        67
      ];
    };
  };

  environment.etc = {
    "dnsmasq.d/02-pihole-custom.conf" = {
      mode = "0444";
      text = ''
        local-ttl=150
        min-cache-ttl=60
        no-hosts
      '';
    };
    "pihole/pihole-FTL.conf" = {
      mode = "0444";
      text = ''
        MAXDBDAYS=365
        RATE_LIMIT=3000/60
      '';
    };
  };

  system.activationScripts.init-pihole-directories = {
    text = ''
      mkdir -p /etc/pihole
      mkdir -p /etc/dnsmasq.d
    '';
  };

  # pihole[29711]:   [i] Creating new gravity databases...
  # pihole[29711]:   [✗] Unable to copy data from /etc/pihole/gravity.db to /etc/pihole/gravity.db_temp
  # pihole[29711]:    [✗] Unable to create gravity database. Please try again later. If the problem persists, please contact support.

  # Fixed by deleting the gravity.db* files in /etc/pihole
  # My assumption is that running this activation script creates empty database files
  # Adding the check beforehand might solve

  system.activationScripts.update-pihole-gravity-lists = {
    text = ''
      if [ ! -e /etc/pihole/gravity.db ]; then
        exit 0
      fi

      # Default List
      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "insert or ignore into adlist (address, enabled) values ('https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts', 1);" || true

      # Suspicious Lists
      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "insert or ignore into adlist (address, enabled) values ('https://raw.githubusercontent.com/PolishFiltersTeam/KADhosts/master/KADhosts.txt', 1);" || true
      ${pkgs.sqlite}/bin/sqlite3 /etc/pihole/gravity.db "insert or ignore into adlist (address, enabled) values ('https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Spam/hosts', 1);" || true
    '';
  };

  system.activationScripts.update-pihole-version = {
    text = ''
      ${pkgs.podman}/bin/podman pull docker.io/pihole/pihole || true
    '';
  };

  systemd = {
    services = {
      optimize-pihole-database = {
        enable = true;
        description = "Optimize and compress the query database";
        serviceConfig = {
          ExecStart = "${pkgs.sqlite}/bin/sqlite3 /etc/pihole/pihole-FTL.db VACUUM;";
          type = "simple";
        };
      };
    };
    timers = {
      optimize-pihole-database = {
        enable = true;
        description = "Optimize and compress the query database";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = "Monthly";
          Unit = "optimize-pihole-database.service";
        };
      };
    };
  };
}
