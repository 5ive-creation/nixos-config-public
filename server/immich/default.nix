{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  ## State and settings are in /var/lib/immich/
  services = {
    immich = {
      enable = true;
      port = 2283;
    };
  };

}
