{
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
  monero_reward_address,
  pkgs,
}:
{
  services = {
    monero = {
      enable = true;
      mining = {
        enable = true;
        address = "${monero_reward_address}";
      };
      rpc = {
        address = "0.0.0.0";
      };
      extraConfig = "confirm-external-bind=1";
    };
  };

  environment.systemPackages = with pkgs; [
    monero-cli
  ];

  networking.firewall.allowedTCPPorts = [ 18081 ];

}
