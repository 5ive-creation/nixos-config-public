{ username, syncthing_data_directory, config }:
{
  users = {
    users = {
      syncthing = {
        isNormalUser = false;
        isSystemUser = true;
        group = "syncthing";
        extraGroups = [ "syncthing" ];
        home = "${syncthing_data_directory}";
        homeMode = "775";
      };
      ${username} = {
        extraGroups = [
          "syncthing"
        ];
      };
    };
  };
  services = {
    syncthing = {
      enable = true;
      guiAddress = "0.0.0.0:8384";
      user = "syncthing";
      dataDir = "${syncthing_data_directory}";
      configDir = "${syncthing_data_directory}/config";
      overrideDevices = true;
      overrideFolders = true;
    };
  };
  systemd = {
    services = {
      syncthing = {
        serviceConfig = {
          Slice = "syncthing.slice";
        };
      };
    };
    slices = {
      syncthing = {
        sliceConfig = {
          # MemoryAccounting = "true";
          # MemoryHigh = "2G";
          CPUAccounting = "true";
          CPUQuota = "30%";
        };
      };
    };
  };
  boot = {
    kernel = {
      sysctl = {
        "net.core.rmem_max" = 2500000;
        "fs.inotify.max_user_watches" = 204800;
      };
    };
  };
  networking = {
    firewall = {
      allowedTCPPorts = [ 8384 22000];
      allowedUDPPorts = [ 22000 21027];
    };
  };
}

