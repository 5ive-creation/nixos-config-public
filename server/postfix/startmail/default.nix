{
  config,
  postfix_root_alias,
  postfix_domain,
  postfix_hostname,
  postfix_password,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  services.postfix = {
    enable = true;
    rootAlias = "${postfix_root_alias}";
    relayHost = "[smtp.startmail.com]:587";
    origin = "${postfix_domain}";
    domain = "${postfix_domain}";
    hostname = "${postfix_hostname}";
    transport = ''
      *   relay:[smtp.startmail.com]:587
    '';
    config = {
      smtp_sasl_auth_enable = "yes";
      smtp_sasl_password_maps = "${postfix_password}";
      smtp_sasl_security_options = "noanonymous";
      smtp_tls_security_level = "encrypt";
      smtp_use_tls = "yes";
    };
  };
}
