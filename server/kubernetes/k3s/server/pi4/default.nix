{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../../server
  ];
  boot.kernelParams = [
    "cgroup_enable=memory"
    "swapaccount=1"
  ];
}
