{
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  imports = [
    ../../k3s
  ];

  services.k3s = {
    role = "agent";
  };
}
