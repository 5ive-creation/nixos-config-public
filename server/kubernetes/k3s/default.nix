{
  k3s_server,
  k3s_token,
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  imports = [
    ./clean-data-directory
  ];
  
  networking.firewall.enable = false;
#  networking.firewall.allowedTCPPorts = [ 6443 ];

  services.k3s = {
    enable = true;
    # From master node cat /var/lib/rancher/k3s/server/node-token
    token = "${k3s_token}";
    serverAddr = "${k3s_server}";
# Only enable on the first master node
#    clusterInit = true;
  };

  environment.systemPackages = [ pkgs.k3s ];
}

