{ 
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
  pkgs,
}:
let
  path-cleaner = pkgs.python3.pkgs.buildPythonApplication {
    pname = "path-cleaner";
    version = "0.0.3";
    propagatedBuildInputs = [
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/path-cleaner.git";
    };
  };
in
{
  systemd.services.clean-k3s-data-dir = {
    enable = true;
    description = "Clean up the /var/lib/rancher/k3s/data/ directory";
    serviceConfig = {
      ExecStart = "${path-cleaner}/bin/path-cleaner --yes /var/lib/rancher/k3s/data/ --exclude /var/lib/rancher/k3s/data/current /var/lib/rancher/k3s/data/previous /var/lib/rancher/k3s/data/.lock";
      Type = "simple";
    };
  };
  systemd.timers.clean-k3s-data-dir = {
    enable = true;
    description = "Clean up the /var/lib/rancher/k3s/data/ directory";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnCalendar = "monthly";
      Unit = "clean-k3s-data-dir.service";
    };
  };
}
