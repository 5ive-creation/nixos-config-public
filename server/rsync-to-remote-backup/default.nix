{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  rsync-to-remote-backup = pkgs.python3.pkgs.buildPythonApplication {
    pname = "rsync-to-remote-backup";
    version = "1.3.0";
    propagatedBuildInputs = [
      pkgs.rsync
      pkgs.openssh
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/rsync-to-remote-backup.git";
    };
  };
in
{
  environment.systemPackages = with pkgs; [
    "${rsync-to-remote-backup}"
  ];

  _module.args.rsync-to-remote-backup = "${rsync-to-remote-backup}";

  systemd = {
    services.rsync-to-remote-backup = {
      enable = true;
      description = "Rsync to configured locations";
      serviceConfig = {
        ExecStart = "${rsync-to-remote-backup}/bin/rsync-to-remote-backup";
        Type = "simple";
      };
    };
    timers.rsync-to-remote-backup = {
      enable = true;
      description = "Rsync to configured locations";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "1:00:00";
        Unit = "rsync-to-remote-backup.service";
      };
    };
  };

}

