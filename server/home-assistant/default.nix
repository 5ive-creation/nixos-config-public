{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  ## State and settings are in /var/lib/hass/
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };

  users.users.nginx.extraGroups = [ "acme" ];

  services = {
    home-assistant = {
      enable = true;
      config = {
        default_config = { };
        http = {
          use_x_forwarded_for = true;
          trusted_proxies = [
            # hostnames don't work. this requires an IP
            "::1"
            "127.0.0.1"
          ];
          ip_ban_enabled = true;
          login_attempts_threshold = 5;
        };
        # /var/lib/hass/automations.yaml
        automation = "!include automations.yaml";
      };
    };
  };

}
