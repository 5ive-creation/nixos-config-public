{ pkgs, modulesPath }:
{
  environment.systemPackages = with pkgs; [
    btdu
    dua
    gdu
  ];
}
