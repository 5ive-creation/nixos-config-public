{
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{

  nixpkgs.config = {
    packageOverrides = pkgs: {
      master =
        import
          (pkgs.fetchFromGitHub {
            owner = "kmogged";
            repo = "nixpkgs";
            rev = "d03eb76bf88633d61ea57655fe39677029d5df37";
            hash = "sha256-AVGrYK/E/IZqa5M2BEbvitRTwN4M1q1p6iONm5+Ff04=";
          })
          {
            inherit (config.nixpkgs) config;
            inherit (pkgs) system;
          };
    };
  };

  environment.systemPackages = [
    pkgs.master.bitrot
  ];

}
