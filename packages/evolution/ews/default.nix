{ pkgs, modulesPath }:
{
  imports = [
    ../default.nix
  ];
  programs.evolution = {
    plugins = [ pkgs.evolution-ews ];
  };
}

