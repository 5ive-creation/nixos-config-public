{
  lib,
  config,
  modulesPath,
  options,
  specialArgs,
}:
{
  # 1. mkdir -p /var/cache/nix-serve
  # 2. nix-store --generate-binary-cache-key cache.example.com /var/cache/nix-serve/priv-key.pem /var/cache/nix-serve/pub-key.pem
  # 3. chown nix-serve -R /var/cache/nix-serve
  # 4. chmod 600 /var/cache/nix-serve/priv-key.pem

  services.nix-serve = {
    enable = true;
    secretKeyFile = "/var/cache/nix-serve/priv-key.pem";
  };

}
