{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
  pkgs,
}:
{
  environment.systemPackages = [
    pkgs.alpaca
  ];
}
