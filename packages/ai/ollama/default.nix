{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  services = {
    ollama = {
      enable = true;
      loadModels = [
        "codellama"
      ];
    };
  };
}
