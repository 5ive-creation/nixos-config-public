{
  pkgs,
  lib,
  nix-update-script
}:
let
  pname = "shell_gpt";
  version = "0.9.0";
  shell-gpt = pkgs.python3.pkgs.buildPythonApplication {
    pname = pname;
    version = version;
    src = pkgs.python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "sha256-KzW9yI1TGG2hFKeXHFqqYCLw/PB9+lJoTgyWrXxCHpo=";
    };

    nativeBuildInputs = with pkgs; [
      python3.pkgs.pythonRelaxDepsHook
      python3
      python3.pkgs.pip
    ];

    propagatedBuildInputs = with pkgs.python3.pkgs; [
      markdown-it-py
      rich
      distro
      typer
      requests
    ];

    pythonRelaxDeps = [ "requests" "rich" "distro" "typer" ];

    passthru.updateScript = nix-update-script { };

    doCheck = false;

    meta = with lib; {
      mainProgram = "sgpt";
      homepage = "https://github.com/TheR1D/shell_gpt";
      description = "Access ChatGPT from your terminal";
      platforms = platforms.unix;
      license = licenses.mit;
      maintainers = with maintainers; [ mglolenstine ];
    };
  };
in
{
  environment.systemPackages = with pkgs; [
    "${shell-gpt}"
  ];
}
