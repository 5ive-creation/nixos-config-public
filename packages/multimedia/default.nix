{
  pkgs,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  imports = [
    ../unfree
  ];
  environment.systemPackages = with pkgs; [
      jellyfin-media-player
      spotify
      freetube
  ];
}
