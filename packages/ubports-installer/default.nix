{ 
  username,
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
let
  # if file -k yields ISO from the appImage use type 1
  ubports_installer = pkgs.appimageTools.wrapType2 {
    name = "ubports-installer";
    src = builtins.fetchurl {
      url = "https://github.com/ubports/ubports-installer/releases/download/0.9.10/ubports-installer_0.9.10-beta_linux_x86_64.AppImage";
      # nix-prefetch-url <URL>
      sha256 = "1caaam7gmpffpyjbh31065f00iyvrax92gh5sbgcpz7shch9r7a9";
    };
  };
  ubports_installer_icon = builtins.fetchGit {
    url = "https://github.com/ubports/ubports-installer.git";
  };
in
{
  environment.systemPackages = with pkgs; [
    "${ubports_installer}"
    (pkgs.makeDesktopItem {
      name = "UBPorts Installer";
      desktopName = "UBPorts Installer";
      genericName = "Utilities";
      icon = "${ubports_installer_icon}/build/icons/512x512.png";
      exec = "${ubports_installer}/bin/ubports_installer";
      terminal = false;
    })
  ];

  programs.adb.enable = true;
  users.users.${username}.extraGroups = ["adbusers"];

}
