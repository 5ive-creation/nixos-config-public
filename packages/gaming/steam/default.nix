{
  pkgs,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  imports = [
    ../../unfree
  ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = false;
  };
}
