{
  pkgs,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  hardware.graphics.enable32Bit = true;
  environment.systemPackages = with pkgs; [
    lutris
  ];
}
