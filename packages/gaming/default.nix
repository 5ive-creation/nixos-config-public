{
  pkgs,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
{
  imports = [
    ./lutris
    ./steam
  ];
  environment.systemPackages = with pkgs; [
      shattered-pixel-dungeon
      wesnoth
      wineWowPackages.waylandFull
  ];
}

