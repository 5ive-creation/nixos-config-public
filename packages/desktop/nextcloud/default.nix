{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  imports = [
    ../../desktop
  ];
  environment.systemPackages = with pkgs; [
    nextcloud-client
  ];
}
