{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  environment.systemPackages = with pkgs; [
    element-desktop
    dig
    gnome-feeds
    mission-center
    tor-browser-bundle-bin
    tribler
  ];
}
