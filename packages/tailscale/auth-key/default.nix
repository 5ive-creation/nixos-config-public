{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  imports = [
    ../../tailscale
  ];

  # https://login.tailscale.com/admin/settings/keys
  # mkdir -p /var/lib/tailscale -m 700 && cat > /var/lib/tailscale/authkey && chmod 600 /var/lib/tailscale/authkey
  services.tailscale = {
    authKeyFile = "/var/lib/tailscale/authkey";
  };

}
