{
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../unfree
  ];

  environment.systemPackages = with pkgs; [
    dnsutils
    freetube
    slack
    spotify
    tailscale
    terraform
  ];
}
