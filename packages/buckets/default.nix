{
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../unfree
  ];

  nixpkgs.config = {
    packageOverrides = pkgs: {
      master =
        import
          (pkgs.fetchFromGitHub {
            owner = "NixOS";
            repo = "nixpkgs";
            rev = "5d6219c83cedec5868aabbd11f914e2b28197881";
            hash = "sha256-uNYu2cDznSm+rj3a8PsmvOHKOFTOAegDFlB5tUijQ+I=";
          })
          {
            inherit (config.nixpkgs) config;
            inherit (pkgs) system;
          };
    };
  };

  environment.systemPackages = [
    pkgs.master.buckets
  ];

}
