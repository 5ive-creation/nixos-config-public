{
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ./zed
  ];
  environment.systemPackages = with pkgs; [
    black
    git
    gnome-builder
    python3
    ruff
    shellcheck
    terraform
    dbeaver-bin
  ];
}
