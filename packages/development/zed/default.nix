{
  pkgs,
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  environment.systemPackages = with pkgs; [
    zed-editor
    nil
    nixd
  ];
}
