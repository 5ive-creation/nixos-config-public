{
  pkgs,
  username,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  virtualisation.libvirtd = {
    enable = true;
    qemu = {
      swtpm.enable = true;
      ovmf.enable = true;
      ovmf.packages = [ pkgs.OVMFFull.fd ];
    };
  };
  programs.virt-manager.enable = true;
  environment.systemPackages = with pkgs; [
      swtpm
  ];
  users.users.${username}.extraGroups = [
    "libvirtd"
  ];
  virtualisation.spiceUSBRedirection.enable = true;
}

