{ username, config, options, specialArgs, lib, modulesPath, }: {
  virtualisation.docker.enable = true;
  users.users.${username}.extraGroups = [ "docker" ];
}
