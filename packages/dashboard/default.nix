{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  imports = [
    ../unfree
  ];
  environment.systemPackages = with pkgs; [
    element-desktop
  ];
}
