{
  pkgs,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
let

  arkenfox = builtins.fetchGit {
    url = "https://github.com/arkenfox/user.js.git";
  };

  override_file = pkgs.writeTextFile {
    name = "user-overrides.js";
    text = builtins.readFile ./user-overrides.js;
  };

  sandbox-firefox-browser = pkgs.writeShellApplication {
    name = "sandbox-firefox-browser";

    runtimeInputs = [
      pkgs.bash
      pkgs.coreutils
      pkgs.firefox
      pkgs.mktemp
    ];

    text = ''
      temp=$(${pkgs.mktemp}/bin/mktemp -d)
      ${pkgs.coreutils}/bin/echo "$temp"
      cd "$temp"

      ${pkgs.coreutils}/bin/cat "${arkenfox}/user.js" > "$temp/user.js"
      ${pkgs.coreutils}/bin/cat "${override_file}"    > "$temp/user-overrides.js"

      ${pkgs.coreutils}/bin/cat "${arkenfox}/updater.sh"      > "$temp/updater.sh"
      ${pkgs.coreutils}/bin/cat "${arkenfox}/prefsCleaner.sh" > "$temp/prefsCleaner.sh"

      ${pkgs.bash}/bin/bash "$temp/updater.sh" -s -u
      ${pkgs.coreutils}/bin/timeout 2 ${pkgs.firefox}/bin/firefox --profile "$temp" --new-instance --no-remote --devtools --private-window "about:blank" || ${pkgs.coreutils}/bin/true

      ${pkgs.bash}/bin/bash "$temp/prefsCleaner.sh" -s
      ${pkgs.firefox}/bin/firefox --profile "$temp" --new-instance --no-remote --devtools --private-window "about:blank"

      ${pkgs.coreutils}/bin/rm -Rf "$temp"
    '';
  };
in
{
  environment.systemPackages = with pkgs; [
    "${sandbox-firefox-browser}"
    (pkgs.makeDesktopItem {
      name = "sandbox-firefox-browser";
      desktopName = "Firefox (Sandbox)";
      genericName = "Web Browser";
      icon = "firefox";
      exec = "${sandbox-firefox-browser}/bin/sandbox-firefox-browser";
      terminal = false;
      #categories = ["Network" "WebBrowser"];
    })
  ];
}
