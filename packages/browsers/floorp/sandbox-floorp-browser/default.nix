{
  pkgs,
  lib,
  config,
  options,
  specialArgs,
  modulesPath,
}:
let

  sandbox-floorp-browser = pkgs.writeShellApplication {
    name = "sandbox-floorp-browser";

    runtimeInputs = [
      pkgs.coreutils
      pkgs.floorp
      pkgs.mktemp
    ];

    text = ''
      temp=$(${pkgs.mktemp}/bin/mktemp -d)
      ${pkgs.coreutils}/bin/echo "$temp"
      cd "$temp"

      ${pkgs.floorp}/bin/floorp --profile "$temp" --new-instance --no-remote --devtools --private-window "about:blank"

      ${pkgs.coreutils}/bin/rm -Rf "$temp"
    '';
  };
in
{
  environment.systemPackages = with pkgs; [
    "${sandbox-floorp-browser}"
    (pkgs.makeDesktopItem {
      name = "sandbox-floorp-browser";
      desktopName = "Floorp (Sandbox)";
      genericName = "Web Browser";
      icon = "floorp";
      exec = "${sandbox-floorp-browser}/bin/sandbox-floorp-browser";
      terminal = false;
      categories = [
        "Network"
        "WebBrowser"
      ];
    })
  ];
}
