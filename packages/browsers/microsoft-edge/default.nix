{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../../unfree
  ];

  environment.systemPackages = with pkgs; [
    microsoft-edge
  ];
}

