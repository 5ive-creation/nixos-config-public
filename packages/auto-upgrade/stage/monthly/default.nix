{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../default.nix
  ];
  system.autoUpgrade = {
    dates = "monthly";
    randomizedDelaySec = "3weeks";
  };
}
