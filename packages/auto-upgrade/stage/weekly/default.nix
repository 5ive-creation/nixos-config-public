{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../default.nix
  ];
  system.autoUpgrade = {
    dates = "weekly";
    randomizedDelaySec = "1week";
  };
}
