{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  system.autoUpgrade = {
    enable = true;
    rebootWindow = {
      lower = "03:00";
      upper = "06:00";
    };
    allowReboot = false;
    operation = "boot";
    randomizedDelaySec = "20min";
  };
  systemd = {
    services.nixos-upgrade = {
      unitConfig = {
        Conflicts = "btr-backup.service";
        After = lib.mkForce "network-online.target btr-backup.service";
      };
    };
  };
}
