{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../default.nix
  ];
  system.autoUpgrade = {
    dates = "*-*-01 03:00:00";
  };
}
