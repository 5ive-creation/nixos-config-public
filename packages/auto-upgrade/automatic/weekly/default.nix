{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{
  imports = [
    ../default.nix
  ];
  system.autoUpgrade = {
    dates = "Sat *-*-* 03:00:00";
  };
}
