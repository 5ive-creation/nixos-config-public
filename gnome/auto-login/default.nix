{
  username,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  services.displayManager.autoLogin = {
    enable = true;
    user = "${username}";
  };
}
