{ }:
let
  nixos_hardware = builtins.fetchGit {
    url = "https://github.com/NixOS/nixos-hardware.git";
  };
in
{
  # Update the bootloader

  environment.systemPackages = [
    # visionfive2-firmware-update-sd
    (pkgs.callPackage
      "${nixos-hardware}/starfive/visionfive/v2/firmware.nix"
      { }).updater-sd
    # visionfive2-firmware-update-flash
    (pkgs.callPackage
      "${nixos-hardware}/starfive/visionfive/v2/firmware.nix"
      { }).updater-flash
  ];
}

