{
  pkgs,
  modulesPath,
  specialArgs,
  options,
  config,
  lib
}:
{
  boot = {
    extraModprobeConfig = ''
      options iwlwifi disable_11ax=true
    '';
  };
}
