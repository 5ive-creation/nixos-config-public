{
  pkgs,
  send-matrix-message,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
let
  check-drive-smart-stats = pkgs.writeShellApplication {
    name = "check-drive-smart-stats";
    runtimeInputs = [
      pkgs.coreutils-full
      pkgs.gawk
      pkgs.gnugrep
      pkgs.hdparm
      pkgs.smartmontools
      pkgs.util-linux
    ];
    text = ''
      for i in $(${pkgs.util-linux}/bin/lsblk | ${pkgs.gnugrep}/bin/grep "^sd" | ${pkgs.gawk}/bin/awk '{print $1;}'); do \
        scan=$(${pkgs.smartmontools}/bin/smartctl --attributes --log=selftest "/dev/$i" | \
               ${pkgs.gnugrep}/bin/grep -P "((Reallocated_Sector_Ct|Current_Pending_Sector).+[1-9]$)" || true)
        if [ -n "$scan" ]; then
          ${pkgs.coreutils-full}/bin/echo "$i - $(${pkgs.hdparm}/bin/hdparm -I "/dev/$i" | grep -i 'serial number')<br/>"
          ${pkgs.smartmontools}/bin/smartctl --attributes --log=selftest "/dev/$i" | \
          ${pkgs.gnugrep}/bin/grep -P '(^ID.+|^sd.+|(Reallocated_Sector_Ct|Current_Pending_Sector).+[1-9]$)'
          ${pkgs.coreutils-full}/bin/echo "<br/>"
        fi
      done \
        2>&1 | ${send-matrix-message}/bin/send-matrix-message --header "check-drive-smart-stats ($(${pkgs.hostname}/bin/hostname))" --message-type m.text
      '';
    };
in
{
  systemd = {
    services.check-drive-smart-stats = {
      enable = true;
      description = "Check drives for errors";
      serviceConfig = {
        ExecStart = "${check-drive-smart-stats}/bin/check-drive-smart-stats";
        Type = "simple";
      };
    };
    timers.check-drive-smart-stats = {
      enable = true;
      description = "Check drives for errors";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "09:04:00";
        Unit = "check-drive-smart-stats.service";
      };
    };
  };
  services.smartd = {
    enable = true;
    notifications.wall.enable = true;
  };
}
