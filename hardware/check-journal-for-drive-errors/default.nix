{
  send-matrix-message,
  pkgs,
  options,
  config,
  specialArgs,
  lib,
  modulesPath,
}:
let
  check-journal-for-drive-errors = pkgs.writeShellApplication {
    name = "check-journal-for-drive-errors";
    runtimeInputs = [ ];
    text = ''
      ${pkgs.systemd}/bin/journalctl --since="$(date -I --date=yesterday)" --quiet --grep 'I\/O error' 2>&1 || ${pkgs.coreutils}/bin/true | \
        ${send-matrix-message}/bin/send-matrix-message --header "check-journal-for-drive-errors ($(${pkgs.hostname}/bin/hostname))"
    '';
  };
in
{

  systemd = {
    services."check-journal-for-drive-errors" = {
      enable = true;
      description = "Check previous day for drive errors in the logs";
      serviceConfig = {
        ExecStart = "${check-journal-for-drive-errors}/bin/check-journal-for-drive-errors";
        Type = "simple";
      };
    };
    timers."check-journal-for-drive-errors" = {
      enable = true;
      description = "Check previous day for drive errors in the logs";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "09:47:00";
        Unit = "check-journal-for-drive-errors.service";
      };
    };
  };
}
