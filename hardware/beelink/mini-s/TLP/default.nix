{
  modulesPath,
  config,
  options,
  specialArgs,
  lib,
}:
{

  # https://linrunner.de/tlp/settings/processor.html
  services = {
    power-profiles-daemon.enable = false;
    tlp = {
      enable = false;
      settings = {
        CPU_DRIVER_OPMODE_ON_AC = "passive";
        CPU_DRIVER_OPMODE_ON_BAT = "passive";

        # Defaults
        # CPU_SCALING_MIN_FREQ_ON_AC  = 800000;
        # CPU_SCALING_MAX_FREQ_ON_AC  = 2900000;
        #
        # CPU_SCALING_MIN_FREQ_ON_BAT = 800000;
        # CPU_SCALING_MAX_FREQ_ON_BAT = 2900000;

        CPU_SCALING_MIN_FREQ_ON_AC = 800000;
        CPU_SCALING_MAX_FREQ_ON_AC = 2000000;

        CPU_SCALING_MIN_FREQ_ON_BAT = 800000;
        CPU_SCALING_MAX_FREQ_ON_BAT = 2000000;

        CPU_BOOST_ON_AC = 0;
        CPU_BOOST_ON_BAT = 0;

        CPU_HWP_DYN_BOOST_ON_AC = 0;
        CPU_HWP_DYN_BOOST_ON_BAT = 0;
      };
    };
  };

}
