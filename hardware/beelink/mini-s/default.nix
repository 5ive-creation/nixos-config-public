{
  modulesPath,
  lib,
  specialArgs,
  options,
  config,
  pkgs,
}:
let
  is_compatible_linux = pkgs.linuxPackages.kernelAtLeast "6.7";
  linuxPackages = if is_compatible_linux then pkgs.linuxPackages else pkgs.linuxPackages_6_11;
in
{
  imports = [
    ./TLP
  ];
  boot.kernelPackages = linuxPackages;
}
