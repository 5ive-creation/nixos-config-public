{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  environment.systemPackages = with pkgs; [
    (pkgs.writeShellApplication {
      name = "calculate-best-dd-bs";
      text = builtins.readFile ./calculate-best-dd-bs.sh;
    })
  ];
}
