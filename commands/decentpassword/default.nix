{
  pkgs,
  config,
  options,
  specialArgs,
  lib,
  modulesPath,
}:
{
  environment.systemPackages = with pkgs; [
    (pkgs.writeShellApplication {
      name = "decentpassword";
      text = ''
        random_chars() {
        tr -dc abcdefghknopqrstuvwxyzCDEFHJKLMNPQRTUVWXY379 < /dev/urandom |head -c4
        }
        echo "$(random_chars)-$(random_chars)-$(random_chars)-$(random_chars)"
      '';
    })
  ];
}
