{ pkgs }:
let
  pipe-if-unseen = pkgs.python3.pkgs.buildPythonApplication {
    pname = "pipe-if-unseen";
    version = "0.0.1";
    propagatedBuildInputs = [
      pkgs.python3.pkgs.lmdb
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.com/5ive-creation/pipe-if-unseen.git";
    };
  };
in
{
  environment.systemPackages = with pkgs; [
    "${pipe-if-unseen}"
  ];

}
